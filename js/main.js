$(document).ready(function() {

    var owl = $("#slider");

    $("a[href='#top']").on('click touch', function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    $("#slider").owlCarousel({
        items: 1,
        responsive: false,
        nav: true
    });
    
    $('.link').on('click touch', function() {
        $(this).closest('.car-block').find('a')[0].click();
    });
});
